---
title: "Prolonged Fasting"
author: ["M. James Kalyan"]
date: 2020-02-11T00:00:00-05:00
draft: false
featured_image: "/images/fasting_thumbnail.png"
---

Fasting is a bit of a health trend these days. Now, I am
fully aware that there are many "health crazies" that do all sorts of crazy things
with questionable evidence in favour of their choices, but if you try a bunch of
things then you are bound to find some that work. Unfortunately, there is much
confusion in the public eye surrounding health research. Food companies act in
their own interest, anti-vaxers serially misinterpret science, alternative
medicine junkies conflate causes and coincidence, et cetera, et cetera. So if you
aren't careful about what information you take seriously, you could end up with
a severe mistrust of science or sheer confusion. However if you spend the time
to vet your sources, there are definitely some that you can trust. A decent rule
of thumb: look for scientists who dedicate their life to a topic. For sleep
science, you might find someone like [Dr. Matthew Walker](https://www.sleepdiplomat.com/professor). For dietary research, you
have a safe bet with people like [Dr. Peter Attia](https://peterattiamd.com/) and [Dr. Rhonda Patrick](https://www.foundmyfitness.com/). Fasting
passes the test with flying colours according to the aforementioned health
researchers[^fn:1]. I do recommend reading their words on the topic rather than
mine--I'm not a medical researcher.

Now that I've (hopefully) convinced you I'm not disregarding science when making
decisions about my health, you can read about my most recent prolonged fasting
experience with an open mind. It should be interesting for those that want to
attempt fasting themselves.


## Day 1 {#day-1}

The last meal I had was yesterday late in the evening. I'm writing this late
in the night after the first day has passed. My metabolism tends to be quite
slow so I don't feel that hungry yet, even though I normally eat a lot (too
much).

I've decided to do a prolonged fast because I haven't done a proper fast in a
while and I feel hungry way too often, which is typically the result of eating
too many carbohydrates. During the fast I'm still able to drink water, but
absolutely nothing else. No tea, no coffee, no nothing. The last time I
undertook a prolonged fast, it was for a 4 day period. I observed that hunger
isn't an entirely accurate feeling--and much like the feeling claustrophobia, it
usually exists as the illusion of danger. For me, after 2 days the pretend
hunger goes away and the real hunger sets in at some time I haven't determined
(because I ate at the four day mark without feeling too hungry).


## Day 2 {#day-2}

Alright! It's 7pm on day 2 and I'm feeling fine, I haven't had a single stomach
grumble yet.

When I eat without restraint, I always feel an uncomfortable dependency on food
that I would rather not have. So I'm going to do a four day fast and log the
experience in minor detail.


## Day 3 {#day-3}

It's the morning of day 3 and I have ended my fast. I intended to do 4 days but
I failed by accident! I was making some custard pudding for my girlfriend and
habitually tasted it to make sure the flavour was correct... Well it was still a
good experience and it reminds me how I shouldn't always listen to my stomach. I
know it's a little anti-climactic, but hey, that's how life is sometimes.

I'd encourage anyone considering fasting to give it a go (as long as there isn't
some medical reason why you should not, check with your doctor first), it's not
as bad as it seems!

[^fn:1]: Peter Attia has [a great page](https://peterattiamd.com/category/fasting/) dedicated to fasting on his website, as does [Rhonda Patrick](https://www.foundmyfitness.com/topics/fasting).
