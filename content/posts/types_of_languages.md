---
title: "Logographic, Syllabic, and Alphabetic Languages"
author: ["M. James Kalyan"]
date: 2020-02-29T00:00:00-05:00
draft: false
featured_image: "/images/types_of_writing_thumbnail.png"
---

I want to talk a bit about three categories of written language: logographic,
syllabic, and alphabetic.

-   Logographic languages have characters which are meant to visually represent a
    word, think of ancient Egyptian hieroglyphics.
-   The characters in syllabic languages represent single syllables. Languages
    such as Korean have this property.
-   I suppose anyone reading this article is familiar with alphabetic languages,
    as English is one. They have alphabets containing characters that represent
    the phonemes[^fn:1] for that language.

Let's start with alphabets. In theory, an alphabet is the most
character-efficient way to build a language, meaning you need less characters to
exist in order to encode every possible word. That's great but alphabets can
also get themselves into quite the mess. Take English for example: English was
unstandardized for most of its existence and it was never really properly
standardized. As such, you commonly find multiple correct spellings of the same
word, redundant characters (I'm looking at you **K** / **C**), and possibly the
worst of it all: ambiguous phonology. Is it _analogue_ or _analog_? _Tonne_ or
_ton_? _neighbour_ or _neighbor_? Why do we need both **C** and **K** in the word
_lock_? It's pronounced no differently than the word _loc_ (if you're Scottish
then there's probably a bone to pick here but as far as the bastardized English
word _loc_ goes, it's the same as _lock_). And when I type _read_ and _read_,
you have no idea whether it's two of the same word, or two different words and
in what order. Of course context works its magic most of the time, but if you've
spoken enough English you know there's too much ambiguity.

This is unrelated to the discussion of alphabets, but English grammar also has
it's pitfalls. Just for fun have a look at the following sentence, how do you
interpret it at first and how many meanings can you deduce?

> I saw a man on a hill with a telescope.

Anyhow, you could certainly overhaul English or any alphabetical language to
invalidate my gripes, but who says we need less characters in the first place?
This is where syllabic languages come in. If you're willing to learn a few more
characters, you can lower the number of characters needed to write a word rather
than the number of characters that exist. Syllabic languages take this approach,
but in a less extreme manner compared to logographic languages.

In my opinion, Korean is a good example of well-designed syllabic language. I'm
not an expert but I once put the time into learning all the characters and it
only took a few days to read words whose meaning I was otherwise oblivious to.
But hey, at least I could pronounce them (poorly)! I've since forgotten most of
that knowledge but I'm confident it wouldn't be hard to relearn. The reason
Korean is so easy to read is that it has a relatively new writing system that
some folks actually sat down and thought about all at once called Hangeul!
Before the completion of Hangeul circa 1443-1444, Koreans used classical Chinese
alongside some phonetic writing systems--kind of like Japan now (with Hiragana,
Katakana, and Kanji), but Kanji, derived from Chinese, deviates enough to be
considered a Japanese script at this point. Hangeul makes Korean interesting
because it's technically an alphabet. It uses the alphabetical consonant and
vowel sounds to construct syllabograms[^fn:2] that are phonemically
consistent. I think an example is better than a description here:

> The word "water" in Korean uses a single syllabogram, 물 (mul). 물 is comprised
> of the consonant ㅁ (m), vowel ㅜ (u), and final consonant ㄹ (l).

With this system you get nice, dense, syllabically-composed words and the
pronunciation fidelity of an alphabet with the added bonus that memorizing all
the characters is as easy as knowing the components.

If you want to take on a whole lot of characters in exchange for the benefit of
super compact words, then logographic languages are the way to go! Consider
written Chinese; many people don't know is that Chinese is actually logographic.
Because Chinese is a bit of a complicated topic, let me restrict what I'm
talking about to what is called "Traditional Chinese", e.g. the script you'd see
walking down the streets of Taiwan or Hong Kong, but not in China where you'd
see "Simplified Chinese". Each Chinese logogram[^fn:3] is composed of
radicals that present an at least somewhat visually intelligible concept. Here I
show you three characters that are visually and meaningfully tied together. You
can see what I mean by "somewhat visually intelligible" in noticing the first
character looks vaguely like a gate (and quite like a saloon door):

> This character 門 means "gate" or "door", you can see it's closely related to
> the characters 開 for "open" and 關 for "closed". The pronunciation for these
> characters depends on what spoken Chinese language or dialect you choose, so
> I'll omit any romanization.

Historically, ancient Chinese was more true to life than it is now; the symbols
reflected the way things really look a little more. But as language evolves, so
does its written form, and now the character for "sun" looks like 日 instead of
🌞. I believe it was at the _Hong Kong Museum of History_ where I was able to
see a fascinating cascade of Chinese characters as they had incrementally
changed over the centuries. I don't have a photo to share but Chinese would
certainly make more sense to you if you'd seen it for yourself.

Alright there's a brief glimpse at a few languages with different writing
systems! Mentioning the character-efficiency and whatnot is just a fun way to
incorporate a metric for comparison into my discussion--I don't really think it
means much. As far as I can tell there is no clear **theorectical** best system.
But I have to say I can think of at least one language due for a modern
overhaul. We can directly observe the result of a thoughtfully produced writing
system in Korea right now and I'm really fond of it. I hope I was able to
provide an appetizing taste of each of these writing systems. Maybe you're
inspired to do a little extra reading or even learn a language with a different
writing system than your own. I'm working on Cantonese myself and I've been told
many times I'm a masochist due to its difficulty... Whatever the case may be, I
think it's rewarding to know a bit more about language!

[^fn:1]: A phoneme is the smallest phonetic unit of a language, like the **A**, **S**, or **T** sounds in Engish.
[^fn:2]: A syllabogram is a syllabic character. 물 is a syllabogram just in the same way that **A** is a letter.
[^fn:3]: Logograms are exactly what you'd expect them to be. 😉
