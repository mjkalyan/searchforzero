---
title: "LBRY: The YouTube replacement you've been waiting for."
author: ["M. James Kalyan"]
date: 2020-02-08T00:00:00-05:00
draft: false
featured_image: "/images/yt_lbry.png"
---

[LBRY](https://lbry.com/) ([pronounced](https://lbry.com/faq/lbry-name) "library") is a decentralized, peer-to-peer digtal
media sharing platform/marketplace with a revenue model surrounding LBC, their
own cryptocurrency, as opposed to traditional advertising. If you're already
excited, you can feel free to stop reading right now and head on over to their
site to start exploring it yourself. If you're not there quite yet, let me share
why this is so exciting to me.

**Decentralization**, a word that should make you feel happy for a few reasons:
On the LBRY network, there is no one party or corporation that owns the
content. Your data is your own, only you can remove it and you get to decide
how/if it is monetized. Many content creators on YouTube have professed the feeling that they
are being held hostage by a monolithic and mysterious entity with ever-shifting
goal posts regarding how to play by the rules--LBRY obliterates this concern
with its decentralized protocol. It also allows content to be filtered by users
instead of corporations, and keeps your data safe at the same time.

**Tipping in the wake of ads** has got to be one of the most attractive things
about LBRY. This is immensely important for reforming the type of media that we
see online. If you sign out of your YouTube account and clear your cache and
cookies before navigating to the front page of YouTube, you will be confronted with
a barrage of videos I have a hard time imagining anyone asked for. This is the
case because those videos are A) something advertisers are willing to put their
mouth behind and B) something tricky or mindless enough to get us to click on.
Many of us have been browsing YouTube only to come to the sudden realization
that _nothing_ we've seen in the past 20 minutes has provided us with any kind
of value. And remember, content creators are incentivised to make these kinds of
videos otherwise they can't make a living. Here's where tipping comes in! If the
consumers are the ones who decide whether some media is worthy of existing, then
creators can foster a community around creating what they want to create in a
no-strings-attached manner. The issue of having viewers pay for content is
solved through LBRY's LBC, the formerly mentioned cryptocurrency. Upon creation
of your LBRY account, you will gain access to your very own LBC wallet. LBRY
will reward you with LBC for using the platform which you can then use to
support the creators you feel deserve it. How great is that? There's no cost to
you outside of your explicit decision to purchase LBC and directly support
content you like. No bank, no middleman, just blockchain.

**A true payment model**: LBRY supports charging people for access to your
content. For example, you might create an educational film series on
photography and wish to release it, but you don't have access to traditional
publishing methods. With LBRY, you can simply be your own publisher. Put it out
there, market it as you please, and the LBRY protocol with handle the paywall as
you set it.

**Discoverability through support** is a concept I haven't mentioned yet.
Essentially, when a creator recieves a tip, it goes into the "support" pool for
their channel. The value of the support pool positiviely effects how the
associated channel gets factored into trending calculations and search results.
If at any time the channel owner wants to redeem LBC from the support pool, they
can do so and the redeemed LBC will show up in their wallet. This is a simple,
yet elegant, way to shape how content is discovered on the platform.

As a new user of LBRY myself, I stumbled across
[a video by the LBRY creators](https://lbry.tv/@lbry:3f/DTF-Feb:e) mentioning that they are currently doing a neat promotional event
they're calling _Down to F\*#$%^ February_. The comically named
[_Down to Follow February_](https://lbry.com/news/2002dtf) is basically an event centered around bolstering
activity on and awareness of LBRY in which all participants get their hands on
some extra LBC. Everyone who joins this month and follows 5 creators will be
rewarded with 15LBC--about 0.3USD as of yet. Large rewards are offered for users
who make videos about LBRY or invite the most new users. And lastly, if the goal
of DTFF is met (50,000 new users register and 500,000 new follows occur on the
LBRY network this February), then bonus LBC is handed out to every new user who
follows 5 channels and every user that has invited a new user.

On top of time restricted promotional events, LBRY also gives out LBC to new
users who were invited to join and to the user who invited them (speaking of
which, I'll just leave my [invite link here](https://lbry.tv/$/invite/@sfz:9)) To me these seem like great ways to
get people to give LBRY a shot, after that the protocol can work its charm and
before you know it LBRY may actually compete with the monolithic YouTube.

If you're interested in all the exciting developments in the LBRY platform and
protocol, you can check out their [2020 roadmap video](https://lbry.tv/@lbry:3f/LBRY-2020-Roadmap-Release:6).

I reserve the right to preach here on my own blog so I'll take that liberty now:
please check out [LBRY](https://lbry.com/) and other people-centric/content-freeing platforms such as the
[Brave](https://brave.com/) web browser. It just makes sense to cut out the middleman if technology
allows it, and now it does! This is especially true for meddling middlemen who
thrive on [appealing to the lowest common denominator](https://lbry.com/news/how-ads-wrecked-entertainment)... I'm looking at you,
television broadcasting and most of the modern internet.
