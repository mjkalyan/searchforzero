---
title: "The Responsibility of the Free World"
author: ["M. James Kalyan"]
date: 2020-04-11T00:00:00-04:00
draft: false
featured_image: "/images/responsibility-freedom-thumbnail.png"
---

By measures such as global warming and the depression epidemic, the world is not
getting better--at least not immediately. But for the most part we are chugging
along the tracks of human progress just fine. We are becoming less violent and
prone to war[^fn:1], less
tribal[^fn:2], less nationalistic, less religious, and more
scientific. One of the most important things progress has yielded is the
empowerment of the individual. In the first world we don't look to monarchs for
holy decrees or cower under the will of warlords, we vote in (hopefully) fair
elections and participate in a democratic policy making process which encodes,
albeit not yet perfectly, the will of the people. It's important to get the
causality right here. It is not that first world countries just happen to become
democratic bastions of individual liberties, countries first respect individual
liberties then enter the first world as a result. The world has become more free
and where it has become more free certain values can be identified as the
causes.

If you were born in a free country as I was, you may be prone to taking such
values for granted. You may expect that if a crime is alleged against you that
you will have the opportunity to prove your innocence in a fair trial. You may
expect that when a crime is committed against you that there is infrastructure
in place to protect your basic human rights. You might not think about your
ability to access education, to vote in fair elections, to associate with
whichever group you see fit, and to participate in public or private discourse
of any kind (including criticism of the government) with impunity. This is the
kind of progress free societies have made[^fn:3].
Unfortunately this is not the case for many places on Earth.

If you were born in a country with restricted personal freedoms and a disregard
for human rights, you may be familiar with the concept that certain ideas are
harmful and should not be discussed, that the law should not be questioned, that
the litany of injustices committed by the government are necessary for the
security of the rest of the population, and that there is nothing telling about
your glorious leader's disembowelment of term limits or farcical electoral
displays (sometimes interspersed with the "disappearance" of political
opponents). In such environments one is likely never to hear a dissenting
opinion publicized in the media. Some of my friends and acquaintances in China
feel the need to keep their mouths shut when it comes to discussing politics due
to the fear of punishment (not just the fear of disrupting a family dinner).
Some embody the _"I'm free if it's not me"_ mentality when it comes to their
government's inhumane practices. Some even deny any shortcomings of their
government whatsoever. Unsurprisingly, these are the loud _unsuppressed_
minority for which the autocracy has "worked". In other words they played by the
dirty rules and got ahead while being forcibly or willfully misinformed
regarding their government's dark side. The confirmation bias at play is similar
to some opinions I have heard out of America regarding their healthcare
situation: "It works now, why do you want to change it?" Clearly, it doesn't
work for the people who want it changed.

This brings us back to the importance of universal personal freedoms.
Considering an injustice against you or your family as equally appalling to an
injustice against a stranger (_"It could be me, so I'm not free"_) is the
mindset that leads us toward a free society.

If you've met enough people from varying backgrounds, you know well enough that
we are all human and we are much more similar than nationalists and
ethno-centrists would have you believe. Upon observation, the differences in the
behaviour and beliefs of other humans become so trivial that it's almost an
unconscious exercise to put yourself in their shoes. One can then understand how
vicious it is to be ripped out of their home, away from their children and
family, and taken to "re-education camps" on the basis of their culture alone
(see [the persecution of Uyghurs](https://en.wikipedia.org/wiki/Uyghurs#Persecution%5Fof%5FUyghurs%5Fin%5FXinjiang)). The result of pondering the trivial nature of
human variance is the emergence of human rights. Because humans do not get to
choose where they are born, what gender they have, what cultural norms they will
be exposed to, what knowledge they will have access to, what intellectual
capacity they have, et cetera, we construct a set of universal rights that
dignify each human equally. I don't even have to argue that all humans deserve
these rights--it's a premise that requires an extreme amount of apathy to
reject.

Then what should those of us in the most free countries do with our freedom? We
should remember the values that provide us such freedom. We should not ignore
the struggles of people who hold the same free values but are under attack by
oppressive autocracies (e.g. the pro-democracy movement in Hong Kong). We have a
voice when countless others do not. We must not allow our greed to cheat fellow
humans out of their freedom. Think international businesses that rely on
violating human rights (directly by exploiting lax worker's rights or indirectly
by kowtowing to tyrannical trading partners) or even something local like the
democracy-undermining corporate lobbyists. We should not allow ideologues to
convince us that freedom comes secondary to a grander goal. These are our
responsibilities.

Put simply: freedom is necessary for upholding human rights, therefore if you
agree that human rights are important, so is freedom. Stand up for it.

[^fn:1]: Steven Pinker makes supports this claim in an all but irrefutable manner in his book _The Better Angels of Our Nature_. This book is astoundingly informative and is a must read as far as I'm concerned.
[^fn:2]: I would argue this is true even in spite of the recent phenomenon of so called "identity politics", a troublesome modern expression of tribalism.
[^fn:3]: Though it is not the topic of this piece, I want to emphasize that free societies as they stand now are not ideal. No country has yet achieved a fully effective democracy rid of "establishment" nepotism and greedy politicians. This **must not** be interpretted as a valid argument against the principles of a free society; it is abundantly clear that moving toward political and economic liberty reduces the frequency and magnitude of injustice. Not only that, but by definition human rights are better upheld by a free society.
