---
title: "Blogs with Hugo and org-mode!"
author: ["M. James Kalyan"]
date: 2020-01-31T00:00:00-05:00
draft: false
featured_image: "/images/emacs+hugo.png"
---

This blog post was written in [emacs](https://www.gnu.org/software/emacs/)' [org-mode](https://orgmode.org/) alongside [Hugo](https://gohugo.io/)!

```org
 * Blogs with Hugo and org-mode!
 CLOSED: <2020-01-31 Fri>
 :PROPERTIES:
 :EXPORT_FILE_NAME: file_name
 :END:
 This blog post was written in [[https://www.gnu.org/software/emacs/][emacs]]' [[https://orgmode.org/][org-mode]] alongside [[https://gohugo.io/][Hugo]]!
```

By making use of org-mode's _org-export-dispatch_ functionality the above org code can be
exported to Hugo compatible markdown. All you need is the wonderful [ox-hugo](https://ox-hugo.scripter.co)
emacs package.

You might be wondering "why would I use org-mode to produce markdown, isn't it
easy enough to just write the markdown?" to which I say, yes, Hugo markdown is
very easy to write and if you don't use emacs--and are happy that way--then I
won't try to convince you to make the switch (though I will say that org-mode,
along with [magit](https://magit.vc/), are the sole reasons why I took up emacs as a vim user). But
if you are an emacs user then you likely already know the power of org-mode and
are probably looking to maximize your time there. But don't take my word for it,
the ox-hugo site has a page entitled _[Why ox-hugo?](https://ox-hugo.scripter.co/doc/why-ox-hugo/)_ which was more than
convincing enough for me.

Mainly, all the nice features of editing text in org-mode now carry over to your
blogging workflow, how nice! Another plus (for some folk) is that you can keep
all your blog posts in one org file as sub-trees. This makes navigating,
searching, and editing your site a breeze.

I'll briefly showcase a few of my favourite features so far:


## Images {#images}

Of course you have image support with the standard org-mode syntax:

```org
#+CAPTION: Don't worry, I'm no Linux elitist, just a Windows hater.
[[~/images/inevitable.jpg]]
```

Which produces this charming depiction of the future:

{{< figure src="/ox-hugo/inevitable.jpg" caption="Figure 1: Don't worry, I'm no Linux elitist, just a Windows hater." >}}


## Tables {#tables}

The universally loved org tables are handled by ox-hugo as well:

```nil
| Nickname | Height | Favourite Food |
|----------+--------+----------------|
| Condor   |    181 | pizza          |
| Speed    |    184 | eggs           |
| Meat     |    188 | vegetables     |
| Darco    |    176 | pizza          |
| Apple    |    186 | chocolate eggs |
```

| Nickname | Height | Favourite Food |
|----------|--------|----------------|
| Condor   | 181    | pizza          |
| Speed    | 184    | eggs           |
| Meat     | 188    | vegetables     |
| Darco    | 176    | pizza          |
| Apple    | 186    | chocolate eggs |

The code here only makes sense if you've used org-tables, otherwise it looks
horrid to type. You can trust me that they're great (and if you don't, look [here](https://orgmode.org/manual/Tables.html)).


## \\(\LaTeX\\) (my favourite 💖) {#latex----my-favourite}

With a little extra configuration (namely, the
[addition of MathJax](https://ox-hugo.scripter.co/doc/equations/#fn:fn-1) into your
Hugo theme) you can even export the \\(\LaTeX\\) code that is supported in org-mode,
which is much nicer than writing pure \\(\LaTeX\\) in most cases. Have a gander:

```latex
\begin{equation}
\frac{\sum_{i=1}^{n}{2i^5}}{\lvert \frac{1}{\sqrt{n}} \rvert} \text{, where } n = 19
\end{equation}
```

\begin{equation}
\frac{\sum\_{i=1}^{n}{2i^5}}{\lvert \frac{1}{\sqrt{n}} \rvert} \text{, where } n = 19
\end{equation}

Ah I really love org-mode... If only I knew it existed during math classes!


## Summary {#summary}

I am just scratching the surface with this workflow but I can already foresee it
being very pleasant in comparison to managing a hugo project with raw markdown.
Not that Hugo and other static site generators aren't great already--because
they are--it's just that org-mode makes everything just that tad bit better.
