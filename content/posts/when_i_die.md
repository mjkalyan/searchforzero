---
title: "When I Die"
author: ["M. James Kalyan"]
date: 2020-03-23T00:00:00-04:00
draft: false
featured_image: "/images/when-i-die-thumbnail.png"
---

Death will eventually find it's way to your door, as it will mine. Everyone's
due for a visit. However I see no reason to be regretful of the death of another
or afraid of your own. After all, the only thing we really have is merely, yet
spectacularly, the experience offered to us by the present moment. One's fears
of a future death or regret of a death gone by are abstractions that some call
"of the mind", meaning they are not experiences offered by one's basal
perceptions but rather experiences created within one's own mind. I'll have to
write more on this topic in another article, but for now let's consider regret
and fear in particular.


## Regret {#regret}

The best way to remember someone after they die is to learn from them. If there
is something to learn from their successes, learn it. Likewise if there is
something to learn from their mistakes. During their life, if you could have
treated that person better, learn from it and act rightly in the present. But
whatever you do, don't dwell on the past--the simple truth of the matter is that
you cannot change it. Any negative thought you might have when considering the
past turns out to be entirely useless--just feel it, and let it go. When you
choose to revisit the past, ensure you are doing so with the intention to gain
something positive.

Each moment is filled with opportunities to be good. If you blind yourself to
them with regret, you'll end up with more to regret, adding fuel to the flame.
Forego the fire altogether and just appreciate life.


## Fear {#fear}

"Why should I fear death? If I am, then death is not. If death is, then I am
not." A poignant quote from Epicurus, an ancient Greek philosopher. Truly, you
are either dead or you are not dead. Death has either happened or it has not.
You're not dead until you are, so why worry?

In Peter Jackson's film adaptation of J.R.R. Tolkien's _The Lord of the Rings_,
as the evil armies of Mordor storm the city of Minas Tirith, Gandalf the White
reminds Pippin that "death is just another path--a path that we all must take".
It seems to me that this statement is true biconditionally, meaning that a path
is just another death as well. Each opportunity missed is now dead. Each
decision made kills the other potential futures. In a way, each moment is the
death of the universe as it was and the birth of the universe as it is. This is,
of course, not a physical claim. It's not as if the universe is destroyed and
built anew infinitely many times per second. It is merely a way to interpret the
transience of the state of the universe.

Now, do you fear the fact that in choosing which colour of shirt to wear you are
snuffing out the lights of the versions of you who choose differently? Of course
not. Those versions are not real, they are just potential futures. In the same
sense, the "you" that you may be afraid to lose upon death is just a potential
future.

There's nothing special about death. It's a happening among happenings. It only
has bearing over a potential future, not over you. You exist only in the here
and now, don't cloud your experience with the fear of death.


## When I Die {#when-i-die}

When I die I don't want anyone to regret _even a single_ past interaction with
me. If you forgot to say goodbye, were overly abrasive the last time we met, or
didn't apologize for something out of pride, frankly, it doesn't matter. It's
all in the past. I just want people to learn from my mistakes/successes, and
move forward. Hanging on to the past just isn't useful. Keep an open mind, make
an honest effort to do the best you can for the world, and appreciate each
moment that you are given to live. That's all.
