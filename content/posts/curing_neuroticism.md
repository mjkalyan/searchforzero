---
title: "On Curing Neuroticism"
author: ["M. James Kalyan"]
date: 2020-04-02T00:00:00-04:00
draft: false
featured_image: "/images/neuroticism-thumbnail.png"
---

Neuroticism: the ever-so-human affliction of feeling inclined towards anxiety,
fear, anger, guilt, loneliness, envy, jealousy, and frustration. It is a
continuous measurement so you'll find yourself somewhere along the way and
likely never completely off the scale. Keep in mind that neuroticism is
considered an immutable personality trait in psychology; when I talk of "curing"
neuroticism, I am referring to the ability to learn, and subsequently train
yourself, to deal with _neurotic thoughts_ so that your level of neuroticism
doesn't burden you or degrade your quality of life. In order to make this
article personally useful to everyone who reads it, I need to do two things
first:

1.  Convince the reader that there is some level of neuroticism within us all.
2.  Convince the reader that having less neurotic thoughts is better, generally.


## We're all a bit loopy {#we-re-all-a-bit-loopy}

Frankly, some of us are privileged to such an extreme that we never feel the
full effect of bearing a neurotic trait or two. Most folks living in first world
countries (or above a certain familial income threshold) wouldn't notice just
how much it would bother them to feel too far above or below room temperature,
or how impatient/picky they are regarding food, or how much of an aversion they
have to getting dirty or doing manual labour. These things, while hidden from
view to the lucky of us (and yes, simple luck is all it is[^fn:1]), are
expressions of neuroticism. The intense urge to scratch a sudden itch, the
distaste of water when it's not at the "right" temperature, the fear of judgment
producing an inability to speak one's mind, even something as trivial as the
fear of spiders is neurotic on some level. It's very easy to fall into the trap
of accepting these things as inevitably part of life. Perhaps you've had the
experience of stretching or massaging a muscle you didn't even know was tight.
The relief afterwards is something that you wouldn't have known was possible
before stumbling across the tight muscle. In the same way, if you go looking for
the things you respond neurotically to, you are taking the first step toward
unearthing freeing states of mind that you didn't know were available to you.


## Less is more {#less-is-more}

If I can make the distinction between feeling itchy and feeling intensely
motivated to scratch such an itch, then I can show the important difference
between experiencing something _as it is_ and the abstract thought we prescribe
to the experience. If the prescription takes the form of an urgent, panicky need
to scratch, then wouldn't we want to minimize that? I'm not saying scratching
itches is bad (or avoiding spiders, or maintaining proper hygiene, etc.), I'm
just saying that if we can minimize the negative nature of our response to
itchiness, we will most definitely improve the quality of our lives.


## What to do about it {#what-to-do-about-it}

So what can we do with our share of neuroticism? One of my most-used examples to
explain this uses the feeling of anger. Say you're angry about something, it
doesn't matter what. Now, realistically, the anger isn't helping you dissipate
the _source_ of the anger at all (therefore it is useless). In fact you only get
angry because it would've scared the other early homosapiens out of stealing
your food again--or some other expression of evolutionary selection. You can
brainstorm a host of reasons why we might've evolved to be sad about breaking a
belonging or jealous of someone stealing your preferred lover. Regardless,
here's what is going on: something happened, then you became angry. Why? The
"cure" for neurotic thinking really is as simple as recognizing that any
justification one might fall back on here is a fallacious appeal to the
omnipotence of evolution. In other words, saying "I'm afraid of heights because
I've evolved to associate height with danger, and therefore I should be" is
falsely claiming that evolution was right about coding this response into you.
Remember that we feel almost no fear driving even though it is a highly
dangerous activity per the numbers. It is clear that we cannot always fall back
on nature if we care about responding to our environment more realistically.
Now, anger is simpler than fear in that it is seldom ever useful, and if it was,
it would be by chance. So with no good reason to be angry we can--as we
hopefully strive to do with any pointless occupation of thought--simply dismiss
the anger.

Of course, like anything in life you want to get better at, this needs to become
a practice. And the fact that the "cure" is conceptually simple doesn't mean no
effort needs to be applied. Continually noticing the emergence of neurotic
thoughts and how they are fundamentally unrelated to their sources is a way of
training your recognition skills such that you become more aware of the
happenings of your mind. With this increased awareness comes the retrospectively
obvious understanding of how silly it is to become angry when the anger is
useless. This, of course, extends to the full range of negative emotions.

In short, winning the battle against neuroticism is akin to opting out of the
fight as soon as you notice you are caught up in it. It is a matter of
interupting the signal between your response to an external event, and the event
itself. With practice you are able to interupt your neurotic response signals
earlier and earlier until their expression in your behaviour is near zero. The
next time you are reminded to take a deep breath in a stressful time, take that
opportunity to do this type of recognition.

[^fn:1]: This point is a simple one to make: if you had been born in the slums of Bangladesh, how would you be where you are now of your own volition? It is a simple matter of luck (or misfortune) to have been born when and where you were, and to have been presented with any subsequent opportunities to make choices effecting your quality of life.
