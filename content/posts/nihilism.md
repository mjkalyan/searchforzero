---
title: "Nihilism: Halfway to the Truth"
author: ["M. James Kalyan"]
date: 2020-02-19T00:00:00-05:00
draft: false
featured_image: "/images/nihilism_thumbnail.png"
---

In my view, nihilism, while itself being a cause of despair and helplessness, is
actually only one logical step away from being something beautiful. To this
point, I've labeled it as halfway to the truth. A nihilist starts
with the same premise as I do: with any amount of careful analysis, the universe
does not present a unified, objective meaning. I would go so far as to say that
proving or disproving this claim is impossible, leaving us in default confusion
about the "purpose of it all". The nihilist takes this and goes on thinking nothing matters and
lives in sorrow, afterall, his happiness wouldn't change a thing. The mistake
the nihilist is making here is thinking that value can only be determined
objectively or his experience is a purely objective one. Yes,
nothing matters _objectively_, but we are conscious, we have _subjective_
experiences! What we think and feel is a matter of our own subjective concern.
One's emotions do not--and really, cannot--matter to the universe, but that
certainly doesn't necessitate one's descent into nihilism. It is nonsensical to
claim that being happy or sad is of the same consequence to you just because it
is of no consequence to an objective purpose, or more accurately, because there
exists no objective purpose. Happiness is happiness, sadness is sadness, by
definition these feelings are considered good and bad, desirable and
undesirable, by _you_.

A recognition of the objective meaninglessness of the universe can be useful in
seeing the world more clearly so as to make the most of it. However, if one gets
stuck on the meaninglessness of objectivity without acknowledging the richness
of subjective experience, they will likely suffer in the clutches of nihilism.

There's a story I love that helps to illustrate different ways of perceiving
the world:

> A rich businessman takes a vacation to a small remote island. He lies upon the
> beach watching boats on the horizon come and go. A nearby fisherman moseys
> ashore bearing a modest amount of fish. The businessman stops the fisherman and
> asks, "Why have you come back with only a few fish? If you fished a while
> longer, you could catch a lot more." The fisherman responds plainly, "I catch
> enough fish to provide for my family. What would I do with more fish?" The
> businessman, confused, replies, "Why, sell them for a bigger boat, you see, to
> catch _even more_ fish!" Once again the fisherman inquires, "What would I do
> with even more fish?" To which the businessman says, "Sell them to get an _even
> bigger_ boat and hire some crew, you could be rich in no time!" "And after I'm
> rich?" the fisherman asks. The businessman pauses quizzically before concluding,
> "Well, I suppose you could retire and spend the rest of your days out fishing in
> your own little boat."

The first step to understanding the fisherman's perspective is realizing that
the size of his boat has no bearing on the outcome of his life. That first step
is one that the businessman has yet to take. But a nihilist understands that
step quite clearly. In fact a nihilist knows that _nothing_ will effect the
objective outcome of _anyone's_ life. So why doesn't the fisherman give up? If
the quality of his life will be the same regardless of what he does, why does he
not just kill himself? Here's the catch: it's not the quality of his life that
is subverted, it is merely the objective outcome (and truly, why care about the
objective outcome?). The way the fisherman experiences life in each moment from
birth until death is the only real concern. This is by nature of experience
being subjective. Any objective concerns are simply false;
objectivity--objective reality--cannot invest itself in anything one way or
another, it can only _be_. Subjectivity not only _is_ but _is lived_, and
importantly, it can generate something fundamentally of your concern: your
experience. Hence the second step is lending credence to subjective
experience, one that nihilists fail to make and in doing so suffer.

In some sense, the nihilist is less deceived than the businessman, but less
aware of reality than the fisherman. Recognizing the meaninglessness of the
journey is only half of the way to enjoying it.

My advice? Learn to enjoy your experience in each moment. It is all you have,
and it is all that matters. Try to be more appreciative and less possessive. Be
open-minded. Seek truth. Perhaps try practicing secular meditation. Be a force
that you believe will help all conscious beings optimize their subjective
experiences. Be contemplative without fear of the future. Be self-reflective
without regret for the past. And don't forget, we're all in this together!
