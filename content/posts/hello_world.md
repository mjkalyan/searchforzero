---
title: "Hello World"
author: ["M. James Kalyan"]
date: 2020-01-30T00:00:00-05:00
draft: false
---

## Welcome! {#welcome}

This is the **_search for zero_**.

In other words, my personal blog where I write about my interests and hope that
you join me and have a bit of fun along the way.


### Topics {#topics}

This site is dedicated to my enthusiasm for what I call the _search for
zero_--or not searching for anything at all. I'm trying to outline a bit of an
unintuitive idea here, which is the desire to stop desiring, the expedition to
exactly where I am, the search for nothing. That's just another way of saying I
am on a quest to be more mindful.

With that general context in mind, my typical topics of interest are:

-   Philosophy - particularly how it should effect our behaviour.
-   Technology - mainly just software I've been using or think is great.
-   Health - I'm interested but no expert. I might post some fitness stuff.

Of course I won't limit myself and you may not know what to expect, but
hopefully it will be of some value to someone! Stay tuned!
