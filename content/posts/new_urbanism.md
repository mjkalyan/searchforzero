---
title: "Redesigning Cities for Humans"
author: ["M. James Kalyan"]
date: 2020-02-27T00:00:00-05:00
draft: false
featured_image: "/images/newurbanism_thumbnail.png"
---

I've been thinking a lot about how we should organize ourselves for a few years
now. Specifically, how we should design cities to be more traversable, energy
efficient, and catalyze a sense of community.

We've had the power to make
civilization-changing infrastructural overhauls very quickly for a
long while, but of course we're human and we get things horribly wrong from time
to time. A particularly bad example of this tendency to err presents itself in
the post World War II "urban sprawl" phenomenon. Urban sprawl, as the name
suggests, is the spreading and flattening of city design characterized by
suburbs that are so far away from any job, school or marketplace that owning a
car--or, in most cases, many cars--is necessary for day-to-day activities. These
environments typically have little to no verticality, famously bad transit
systems, huge parking lots, and an ever-increasing amount of road infrastructure
which chokes out any developments for pedestrian traffic. In fact, road
infrastructure leads to a _regression_ in existing pedestrian infrastructure:
sidewalks get narrower or disappear, crosswalks and pedestrian lights become so
infrequent that people are tempted to cross multi-lane streets on their
own terms, and so on. I myself live a _ten minute_ walk from the nearest
sidewalk, it's so absurd that it's hilarious. The urban sprawl development
paradigm just doesn't sit right with me, but since I live in North America
there's so much space that the temptation is just to keep building outwards.

If I think about what makes a city great to live in, I think of not having to
travel far to get anywhere. I think of the infusion of nature and beautiful
public spaces everywhere. I think of my friends living just a few transit stops
away. I think of taking a short walk to pick up some groceries from the local
market. I think of kids roaming, playing, and going to school in their own
neighbourhood without their parents having to drive them anywhere. This does not
happen with the presence of flat, segmented, homogeneous, gray--dare I say
it--desolate urban sprawl.

I'm fairly confident no one is really going to disagree that urban sprawl is a
problem that needs fixing, but if you do, you can easily find testimony of
how bad urban sprawl environments are to live in from others. A man from Los
Angeles who has been living abroad in various countries for decades mentioned
that he couldn't quite put his finger on why he was scared to move back to the
USA until he heard people talk about urban sprawl, then it suddenly clicked. By
his account, the countries he had learned to call home fostered a sense of
community. He knew his neighbours, he could walk to get groceries, there was a
healthy presence of nature and recreational areas, and this was no accident--the
cities he was living in didn't have a fractured, hyper-segregated design. I
think this is a hint as to what the first step to building cities that truly
work for humans is: we need to recognize that urban sprawl is at the heart of the
issue. So, where do we go from here? I believe the answer can be found in the
"New Urbanism" movement[^fn:1].


## New Urbanism {#new-urbanism}

I stumbled upon New Urbanism recently, which attempts to address the issues
caused by urban sprawl. For years I had been pondering what ideas to take from
different cities, what works and what doesn't, but I have no formal
understanding of how cities work, I was just using my intuition. As it turns
out, I had been unknowingly parroting some of the conclusions of experts at the
helm of the New Urbanism movement. Here are the things I could deduce myself:

-   Make cities **walkable** and bikeable
-   Create an incredible **transit** system
-   Build **denser** cities, take advantage of **height**
-   **Beautify** the city with nature and human-centric design (why does everything
    revolve around cars???)
-   Make **cars unnecessary** or even insensible to own
-   **Eliminate homogeneity** by making every area diverse (i.e. have shops,
    housing, recreation, and other services). No more copy and pasted suburban
    developments.
-   Build **smaller city blocks**

Intuition can be a very powerful tool and the fact that it brought me to a New
Urbanism is testament to how unintuitive--and frankly, un-human--suburbs and
sprawled out city designs are. However, the New Urbanism advocates have also
come up with some counter-intuitive principles that I wouldn't have thought of
myself. For example, conveniently, designing for narrower roads actually
decreases traffic congestion. This is because "desiging for narrower roads"
includes some of the above points that make it easier to commute live closer to
where you need to be. Contrapositively, more lanes and wider roads make traffic
worse... More people move farther away or get work in the city when they live
elsewhere but those cars all have to end up in the city somehow.

With regards to the question of energy efficiency I think it's quite clear that
density is a big winner. We consume less energy with our water systems, when
transporting goods, commuting, and pretty much everything else when we live
closer together. Just think of the energy consumption difference between heating
a hundred separate houses and heating one tall apartment building.

Considering finances, a large portion of tax spending goes towards road
infrastructure, we could essentially eliminate that sinkhole with a New Urbanism
approach. And no, it would not be opening up an equivalent sinkhole somewhere
else. Building pedestrian infrastructure is simply a smaller project, less
material and time is needed. Bikes and feet cause essentially zero damage to
pathways and non-car traffic is almost impossible to congest so you won't be
fighting the unwinnable car traffic war.

Possibly my favourite reason to move towards a more dense and connected society
is not that it's more practical in just about every way, but that it's _healthier_
for us as humans. I know it's an overused cliché, but I think that the phrase
"money can't buy happiness" is applicable for debunking urban sprawl. A lot of
people think that with a big enough house far enough from everything else (and a
handful of nice cars) they'll be happy, but that's simply not true. What they
find is isolation and inconvenience, with the former being a large coefficient
for depression. We are social beings and being part of a community is far more
important than I think most people give it credit.

After seeing some of my thoughts on this subject improved upon, formalized, and
even applied in places like Copenhagen, I have a newfound optimism for the
future and I'm excited to see just how optimal human life can be!


## Video References {#video-references}

Check out this amazing video by Oscar Boyson related to the topic, it's so well
done you wouldn't expect to be getting educated while watching it.
{{< youtube xOOWk5yCMMs >}}
For a more expert take and the TED talk style of presention, have a look at
these two talks by [Peter Calthorpe](https://www.youtube.com/watch?v=IFjD3NMv6Kw) and [Jeff Speck](https://www.youtube.com/watch?v=6cL5Nud8d7w).

[^fn:1]: Read more about New Urbanism on [Wikipedia](https://en.wikipedia.org/wiki/New%5FUrbanism). These guys have put a lot more thought and expertise into it than me and it's honestly fascinating to learn how good cities can be (and a bit of a bummer if you make comparisons to your own city).
