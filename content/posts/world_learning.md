---
title: "I Learned Every Country in One Day"
author: ["M. James Kalyan"]
date: 2020-02-13T00:00:00-05:00
draft: false
featured_image: "/images/world_thumbnail.png"
---

Yesterday I sat down and studied the location of every country on Earth. I'm not
particularly good at memorization but for some reason I didn't find this task
that difficult. I was lucky enough to stumble upon [lizardpoint](https://www.lizardpoint.com), a website with
nice interactive map quizzes. First I started by learning the countries by
continent, and once I could look at the unlabeled map and list off
each country comfortably for each continent, I moved on to the world map, for
which I did the same. I'm likely to mess up Slovenia and Slovakia some time in
the future, but in just 24 hours I have improved my knowledge of global
geography by a vast margin. This is probably a laughable and childish task to
some people who learn this in grade school, but sadly I did not. And even if
this was a topic during my childhood schooling, I wouldn't have been interested.

This article isn't actually about me or some silly topic I decided to learn
yesterday... I want to make a point about education in general.

Learning out of interest as an adult is such an enjoyable experience. I think
it's sad that many education systems around the world consistently kill the desire to learn
in children, which manifests as the "I hate school" sentiment. Almost everyone who
has graduated secondary school looks back and confirms to themself that they indeed
did not need to learn many topics to do what they are doing now.
Even academics wish they didn't have to waste time learning fields that they are
decisively uninterested in (e.g. myself in English literature courses).

I understand that a kid doesn't necessarily know what they are interested in, but
that's a horrible argument for structuring curricula the way we do.
We don't allow and encourage kids to study topics that they are interested
in, we care about grades over how much a kid is truly learning, and
we glorify some types of knowledge over others (think lawyer versus plumber:
both are fundamentally respectable jobs but most parents would rather their kid
choose the former path). Incoherently, we do all this while pretending one can only learn
as a child. Instead of seeing childhood as a the only time people can learn about
geography or literature, why aren't we treating it as a precious time to
teach children real life lessons? To me, school too often materializes as an
enormous missed opportunity. Forcing kids
to learn everything is a surefire way to disillusion a large portion of them.

Of course I'm not arguing against such basic education as literacy or basic
mathematics, my point is that our educational priorities are misguided and
shortsighted. I would rather teach kids _how_ to think rather than _what_ to
think, given the fluidity and transience of information.

I don't know what sparked my sudden urge to learn the exact locations of each
country in the world, but I do know that learning as an adult is entirely
possible. We should focus on giving kids the _ability_ and _passion_ to learn
rather than the idea that this is the only chance to learn they'll ever get and
that they'd better be an accountant not a musician.
