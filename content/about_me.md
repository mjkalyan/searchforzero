---
title: About Me
featured_image: "images/sfz_web_cover.png"
omit_header_text: false
description: If you want to know me a little better 😉
type: page
menu: main
---

## What is _search for zero_?
_search for zero_ is my personal, casual, and hopefully educational blog where I
discuss any topic I'm interested in. The name _search for zero_ is in reference
to the contentedness found in not searching for anything.

## Who am I?
I'm James! I'm attempting to leave a positive mark on the world by spreading
what I learn about life as far I can and learning the best I can from others. I
would hope to say that I have a large capacity for openness to new ideas and as
such I encourage any and all new information.

My primary interests are philosophy, health, and _responsible_ technology! There
are so many topics to explore and endless things to learn about, but there are
also endless things to despair if you put your mind to it. In the midst of all
the great and wretched things, we should bear in mind our life is lived only
once and that each moment is truly precious. If you follow the logic of this
preposition a little further, you may be drawn to the conclusion that you should
actively learn how to appreciate the moment you are in--after all, now is the
last time you will experience it. That line of reasoning brought me towards
philosophical topics like stoicism and meditation, though not through formal
study and not with interest in the entire field of philosophy. I am simply
interested in the well-being of humans, in other words, the optimization of the
human condition.

You can find my videos on LBRY ([@sfz](https://lbry.tv/@sfz:9)) -- the
ambitious, decentralized, and community driven media sharing platform/YouTube
~~replacement~~ slayer! If [LBRY](https://lbry.com/) is appealing to you, you
can use [my invite link](https://lbry.tv/$/invite/@sfz:9) to support me and get
a little bonus of your own.

## Contact
I'm happy to respond to any messages with nearly any topic. Don't be shy! Email
or either of my _search for
zero_ chatrooms on [Matrix](https://matrix.org/) /
[Discord](https://discordapp.com/) are the best ways to reach me.

- {{< icon matrix-logo >}}
  [Matrix](https://matrix.to/#/!hLtnsvRTjdLaqfDuIP:matrix.org?via=matrix.org)
  / [Discord](https://discord.gg/g4QwMZK) {{< icon discord_icon >}} chatrooms
  (bridging coming later)

- {{< icon email >}} james@searchforzero.com | mjk@searchforzero.com
