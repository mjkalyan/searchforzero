---
title: "search for zero"
featured_image: '/images/sfz_web_cover.png'
description: "with James"
---
{{< rawhtml >}}
<p style="font-size:16px" id="quote"></p>
    <script>
        var quotes = [
        "\"There is nothing passive about mindfulness. One might even say that it expresses a specific kind of passion—a passion for discerning what is subjectively real in every moment.\"<br/>- <i>Sam Harris</i>",
        "\"Being mindful is not a matter of thinking more clearly about experience; it is the act of experiencing more clearly, including the arising of thoughts themselves.\"<br/><i>- Sam Harris</i>",
        "\"How we pay attention to the present moment largely determines the character of our experience and, therefore, the quality of our lives.\"<br/><i>- Sam Harris</i>",
        "\"It's better to be a warrior in a garden than a gardener in a war.\"<br/><i>- Unknown</i>",
        "\"You can pursue happiness, or you can be happy.\"<br/><i>- Unknown</i>",
        "\"You must transcend culture and race to become human.\"<br/><i>- Unknown</i>",
        "\"Being happy is not an acknowledgment that all in the world is well.\"<br/><i>- Unknown</i>",
        "\"It's impossible to be anything tomorrow, become it today.\"<br/><i>- Unknown</i>",
        "\"Embrace the fact that the you of yesterday is dead, then you will be free.\"<br/><i>- Unknown</i>",
        "\"Our minds are all we have. They are all we have ever had. And they are all we can offer others.\"<br/><i>- Sam Harris</i>",
        "\"This is the real secret of life--to be completely engaged with what you are doing in the here and now. And instead of calling it work, realize it is play.\"<br/><i>- Alan Watts</i>",
        "\"I have realized that the past and future are real illusions, that they exist in the present, which is what there is and all there is.\"<br/><i>- Alan Watts</i>",
        "\"Things are as they are. Looking out into the universe at night, we make no comparisons between right and wrong stars, nor between well and badly arranged constellations.\"<br/><i>- Alan Watts</i>",
        "\"You must not be afraid of playing wrong notes. Just forget it. Play it wrong. And then go over it again and eventually you'll get it right.\"<br/><i>- Alan Watts</i>",
        "\"A good traveler has no fixed plans and is not intent on arriving.\"<br/><i>- 老子 (Lao Zi)</i>",
        "\"When I let go of what I am, I become what I might be.\"<br/><i>- 老子 (Lao Zi)</i>",
        "\"Everything has beauty, but not everyone sees it.\"<br/><i>- 孔夫子 (Confucius)</i>",
        "\"The man who asks a question is a fool for a minute, the man who does not ask is a fool for life.\"<br/><i>- 孔夫子 (Confucius)</i>",
        "\"Stop leaving and you will arrive. Stop searching and you will see. Stop running away and you will be found.\"<br/><i>- 老子 (Lao Zi)</i>",
        "\"The best revenge is to be unlike him who performed the injury.\"<br/><i>- Marcus Aurelius</i>",
        "\"If someone is able to show me that what I think or do is not right, I will happily change, for I seek the truth, by which no one was ever truly harmed. It is the person who continues in his self-deception and ignorance who is harmed.\"</br><i>- Marcus Aurelius</i>",
        "\"Very little is needed to make a happy life; it is all within yourself in your way of thinking.\"<br/><i>- Marcus Aurelius</i>",
        "\"Do not act as if you were going to live ten thousand years. Death hangs over you. While you live, while it is in your power, be good.\"<br/><i>- Marcus Aurelius</i>",
        "\"If a man knows not which port he sails, no wind is favourable.\"<br/><i>- Seneca</i>",
        "\"Religion is regarded by the common people as true, by the wise as false, and by rulers as useful.\"<br/><i>- Seneca</i>",
        "\"Difficulties strengthen the mind, as labour does the body.\"<br/><i>- Seneca</i>",
        "\"Guilt is a manifestation of condemnation or aversion towards oneself, which does not understand the changing transformative quality of mind.\"<br/><i>- Joseph Goldstein</i>",
        "\"Every time we become aware of a thought, as opposed to being lost in a thought, we experience that opening of the mind.\"<br/><i>- Joseph Goldstein</i>",
        "\"Just as the light of a single candle can dispel the darkness of a thousand years, the moment we light a single candle of wisdom, no matter how long or deep our confusion, ignorance is dispelled.\"<br/><i>- Joseph Goldstein</i>",
        "\"Generosity, love, compassion, or devotion<br/>do not depend on a high IQ.\"<br/><i>- Joseph Goldstein</i>",
        "\"Hatred never ceases by hatred; it only ceases by love.\"<br/><i>- Joseph Goldstein</i>",
        "\"Without the steadiness of concentration, it is easy to get caught up in the feelings, perceptions, and thoughts as they arise. We take them to be self and get carried away by trains of association and reactivity.\"<br/><i>- Joseph Goldstein</i>",
        "\"Don't gain the world and lose your soul;<br/>wisdom is better than silver or gold.\"<br/><i>- Bob Marley</i>",
        "\"Some people feel the rain. Others just get wet.\"<br/><i>- Bob Marley</i>",
        "\"Wealth consists not in having great possessions,<br/>but in having few wants.\"<br/><i>- Epictetus</i>",
        "\"Seek not greater wealth, but simpler pleasure;<br/>not higher fortune, but deeper felicity.\"<br/><i>- Mahatma Gandhi</i>",
        "\"I believe that a man is the strongest soldier<br/>for daring to die unarmed.\"<br/><i>- Mahatma Gandhi</i>",
        "\"There is more to life than increasing its speed.\"<br/><i>- Mahatma Gandhi</i>",
        "\"Truth never damages a cause that is just.\"<br/><i>- Mahatma Gandhi</i>",
        "\"To believe in something, and not to live it, is dishonest.\"<br/><i>- Mahatma Gandhi</i>",
        "\"The aptitude for playing chess is nothing more than the aptitude for playing chess.\"<br/><i>- Garry Kasparov</i>",
        "\"Simplicity is a difficult thing to achieve.\"<br/><i>- Charlie Chaplin</i>",
        "\"Nothing is permanent in this wicked world – not even our troubles.\"<br/><i>- Charlie Chaplin</i>",
        "\"If you're really truthful with yourself, it's a wonderful guidance.\"<br/><i>- Charlie Chaplin</i>",
        "\"You can preach a better sermon with your life than with your lips.\"<br/><i>- Oliver Goldsmith</i>",
        "\"All we have to decide is what to do with the time that is given us.\"<br/><i>- Gandalf the Grey</i>",
        "\"I would rather have questions that can't be answered than answers that can't be questioned.\"<br/><i>- Richard Feynman</i>",
        "\"Too often we enjoy the comfort of opinion without the discomfort of thought.\"<br/><i>- John F. Kennedy</i>",
        "\"I wish everyone could get rich and famous and everything they ever dreamed of so they can see that's not the answer.\"<br/><i>- Jim Carrey</i>",
        "\"Why should I fear death? If I am, then death is not.<br/>If death is, then I am not.\"<br/><i>- Epicurus</i>",
        "\"The knowledge that nothing matters, while accurate, gets you nowhere.\"<br/><i>- Dan Harmon</i>",
        "\"Experience is not what happens to a man; it is what a man does with what happens to him.\"<br/><i>- Aldous Huxley</i>",
        "\"Keep your head up in failure, and your head down in success.\"<br/><i>- Jerry Seinfeld</i>",
        "\"When you're dead, you're dead - but you're not quite so dead if you contribute something.\"<br/><i>- John Dunsworth</i>",
        ];
        var index = Math.floor((Math.random() * quotes.length)); 
        document.getElementById('quote').innerHTML = quotes[index];
    </script>
{{< /rawhtml >}}
